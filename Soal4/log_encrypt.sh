#!/bin/bash

cp /var/log/syslog /home/sarahnrhsna/Documents/Soal4/"$(date +"%H:%M") $(date +"%d-%m-%Y")".txt
# input=$(date +%k:%M %d-%m-%Y)_encrypt.txt
input=/home/sarahnrhsna/Documents/Soal4/"$(date +"%H:%M") $(date +"%d-%m-%Y")".txt
encoded_file="$(date +"%H:%M") $(date +"%d-%m-%Y")"_encrypt.txt
hour=$(date +"%H")
lowercase=(abcdefghijklmnopqrstuvwxyz)
uppercase=(ABCDEFGHIJKLMNOPQRSTUVWXYZ)

encoded_lowercase="${lowercase=:hour}${lowercase=:0:$hour}"
encoded_uppercase="${uppercase=:hour}${uppercase=:0:$hour}"

while read -r line
do 
    encoded_script=$(echo "$line" | tr "${lowercase[*]}" "${encoded_lowercase}" && echo "$line" | tr "${uppercase[*]}" "${encoded_uppercase}")
    echo "$encoded_script" >> "$encoded_file"
done < "$input" > "$encoded_file"